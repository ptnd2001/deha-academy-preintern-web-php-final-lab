<?php
function validateField($request, $key){
    return isset($request[$key]) && $request[$key] != "" ? "" : "$key is required";
}

function validate($request, $keys){
    $results = [];
    foreach ($keys as $key){
        $errors = validateField($request, $key);
        if($errors != ""){
            $results[$key] = $errors;
        }
    }
    return $results;
}