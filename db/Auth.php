<?php
include_once __DIR__.'./DB.php';
class Auth
{
    protected $hiddens = ['password'];

    public static function login($username, $password)
    {
        // Lấy thông tin user
        // Check: Nếu có user thì lưu vào session return true else return false
        $user = self::getUser($username, $password);
        if(count($user) > 0){
            $_SESSION['user'] = $username;
            return true;
        }
        else {
            return false;
        }
    }

    public static function logout()
    {
        // Xóa session user
        unset($_SESSION['user']);
    }

    public static function check() 
    {
        // check session user tồn tại hay ko
        return ((!isset($_SESSION['user'])) && count($_SESSION['user']));
    }


    public static function getUser($username, $password) 
    //attemp
    {
        $sql = "select username from user where username = :username and password = :password LIMIT 1";
        $dataCheck['username'] = $username;
        $dataCheck['password'] = md5($password);
        $user = DB::excute($sql,$dataCheck);
        return $user;
    }
}