<?php
// session_start();
class DB
{
    static protected $connection;
    const DB_TYPE = "mysql";
    const DB_HOST = "localhost";
    const DB_NAME = "tgdd";
    CONST USER_NAME = "root";
    CONST USER_PASSWORD = "";

    static public function getConnection(){
        // Kết nối tới MySQL
        if(static::$connection == null){
            try{
                static::$connection = new PDO(self::DB_TYPE. ":host=" .self::DB_HOST. ";dbname=" .self::DB_NAME,self::USER_NAME, self::USER_PASSWORD);
            }
            catch (Exception $exception){
                throw new Exception(" connection fail");
            }
        }
        return static::$connection;
    }

    static public function excute($sql, $data = null){
        $statement = self::getConnection()->prepare($sql);
        $statement->setFetchMode(PDO::FETCH_ASSOC);

        //Gán giá trị $data vào và thực thi
        $statement->execute($data);
        $result = [];

        While($item = $statement->fetch()){
            $result[] = $item;
        }
        return  $result;
    }
}