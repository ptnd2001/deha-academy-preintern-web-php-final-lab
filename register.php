<?php
    include_once "./db/user_register.php";
    include_once __DIR__.'./db/DB.php';
    $errors = [];
    
    if (isset($_POST['dangky'])){
        $comfirmPass = $_POST['passwordComf'];
        $pass = $_POST['password'];
        $user = $_POST['username'];
        $phone = $_POST['phone'];
        $justNums = preg_replace("/[^0-9]/", '', $phone);
        $sql_username = "select * from user where username = '" .$user. "' ";
        $user_number = DB::excute($sql_username);
        $number = count($user_number);
        if(count($user_number) > 0) {  
            echo "Tên đăng nhập này đã có người dùng. Vui lòng chọn tên đăng nhập khác. <a href='javascript: history.go(-1)'>Trở lại</a>";
            exit;
        }
        else if(strlen($justNums) != 10){
            echo "Số điện thoại không hợp lệ. Vui lòng nhập lại số điện thoại gồm 10 số!<a href='javascript: history.go(-1)'>Trở lại</a>";
            exit;
        }

        else if(strcasecmp($pass,$comfirmPass) != 0){
            echo "Xác nhận mật khẩu không trùng khớp!<a href='javascript: history.go(-1)'>Trở lại</a>";
            exit;
         }

        else if(strcasecmp($pass,$comfirmPass)==0){
        $errors = validate($_POST, [ 'username', 'password', 'name', 'birth', 'phone']);       
        if(count($errors) <= 0){
            $dataCreate = [
                'username' => $_POST['username'],
                'password' => md5($_POST['passwordComf']),
                'name' => $_POST['name'],
                'birth' => $_POST['birth'],
                'phone' => $_POST['phone'],
            ];
            
            $user = User::create($dataCreate);
                echo "
                    <script>
                        alert('Đăng ký thành công!');
                    </script>
                    ";
        }
        }
        // header('location:login.php');
    }
    

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Đăng ký tài khoản</title>
     <!--important link source from "https://bootsnipp.com/snippets/qNB2D"-->
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

    <link href="https://fonts.googleapis.com/css?family=Quicksand:300,400,500,600,700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="./asscess/css/Home.css" />
    <style>
        html{
            font-size: 16px;
        }
    </style>
</head>
<body>
<section class="ready__started project__form" style="margin-top: 20px">
        <div class="container">
            <h3 class="text-center">Đăng ký tài khoản</h3>
            <div class="ready__started-box">
                <form class="main__form" method="POST">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="username">Tên Đăng Nhập*</label>
                                <input type="text" class="form-control" name="username" aria-describedby="username" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="password">Mật Khẩu*</label>
                                <input type="password" class="form-control" name="password" aria-describedby="password" placeholder="" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="passwordComf">Nhập Lại Mật Khẩu*</label>
                                <input type="password" class="form-control" name="passwordComf" aria-describedby="passwordComf" placeholder="" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="name">Họ Tên*</label>
                                <input type="text" class="form-control" name="name" aria-describedby="name" placeholder="Nguyễn Văn A" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                    <div class="row" style="margin-left: 10px; margin-right: 30px;>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="birth">Ngày Sinh*</label>
                                <input type="date" class="form-control" name="birth" aria-describedby="birth" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="phone">Số điện thoại*</label>
                                <input type="text" class="form-control" name="phone" aria-describedby="phone" placeholder="xxx-xxx-xxxx" required>
                            </div>
                        </div>
                    </div>
                    <div class="text-center" style="margin-top:50px;">
                        <input type="submit" class=" btn btn-get" name = "dangky"></input>
                        <?php require './db/user.php'; ?>
                        <a href="login.php">Quay lại</a>
                    </div>
                </form>
            </div>
        </div>
    </section>
</body>
</html>