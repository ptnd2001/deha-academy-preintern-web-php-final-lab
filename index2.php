<?php
            session_start();
			include_once "./db/Auth.php";			
            if (isset($_POST['logout'])){
                Auth::logout();
                header("location:login_auth.php");
            }
?>
<?php
    include_once "./db/phone.php";
    $phones = phone::all();
    $number_page = phone::getNumberPage();
    $current_page =  phone::getCurrentPage();
?>

<!DOCTYPE html>
<meta charset="utf-8"> 
<html>
    <head>      
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick-theme.min.css"   integrity="sha512-17EgCFERpgZKcm0j0fEq1YCJuyAWdz9KUtv1EjVuaOz8pDnh/0nZxmU6BBXwaaxqoi9PQXnRWqlcDB027hgv9A==" crossorigin="anonymous" referrerpolicy="no-referrer" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.css" integrity="sha512-yHknP1/AwR+yx26cB1y0cjvQUMvEa2PFzt1c9LlS4pRQ5NOTZFWbhBig+X9G9eYW/8m0/4OXNx8pxJ6z57x0dw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" integrity="sha512-5A8nwdMOWrSz20fDsjczgUidUBR8liPYU+WymTZP1lmY9G6Oc7HlZv156XqnsgNUzTyMefFTcsFH/tnJE/+xBg==" crossorigin="anonymous" referrerpolicy="no-referrer" />
        <link rel="stylesheet" href="./fontawesome-free-6.1.1-web/css/all.min.css">
        <link rel="stylesheet" href="./css/base2.css">
        <link rel="stylesheet" href="../asset/js_tgdd.html">
        <link rel="stylesheet" href="./css/style2.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.js" integrity="sha512-HGOnQO9+SP1V92SrtZfjqxxtLmVzqZpjFFekvzZVWoiASSQgSr4cw9Kqd2+l8Llp4Gm0G8GIFJ4ddwZilcdb8A==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.js" integrity="sha512-eP8DK17a+MOcKHXC5Yrqzd8WI5WKh6F1TIk5QZ/8Lbv+8ssblcz7oGC8ZmQ/ZSAPa7ZmsCU4e/hcovqR8jfJqA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
        
    </head>
    <body>      
        <div id="main">
            <div class="img-slider-full">
                <div class="img-slider slider-banner">
                    <div class="image-item">
                        <img src="./img/sale1.png" alt="">
                    </div>
                    <div class="image-item">
                        <img src="./img/sale2.png" alt="">
                    </div>                
                </div>
                <form method="POST">
						<input type="submit" name="logout" value="Logout" class="">
                </form>

                <!-- <div class="btn-logOut" style="margin-top: 20px; float: right; text-decoration: none;font-weight: 550; padding: 8px 4px 8px 0px; color: black; background-color: rgb(239, 166, 8); border-radius: 3px;">
                    <!-- <a href="" style="text-decoration: none; color: black;">Đăng xuất</a> -->
                <!-- <a href="<?php  ?>" style="text-decoration: none; color: black;">Đăng nhập</a>
                </div>
                <div class="btn-logOut" style="margin-top: 20px; float: right; text-decoration: none;font-weight: 550; padding: 8px 4px 8px 0px; color: black; background-color: rgb(239, 166, 8); border-radius: 3px;"> -->
                    <!-- <a href="" style="text-decoration: none; color: black;">Đăng xuất</a> -->
                    <!-- <a href="<?php //Auth::logout(); ?>" style="text-decoration: none; color: black;">Đăng xuất</a> //form gọi hàm logout
                </div> --> 
                
            </div>

            <div class="header-full">
                <div class="header">
                <div class="header-top">
                    <a href="index2.php">
                        <img class="header-top_logo" src="./img/logo.png" alt="">
                    </a>
                    <div class="header-top_address">
                        <a href="">Xem giá, 
                               <br>tồn kho tại: </a>
                        <i class="fa-solid fa-caret-down"></i>
                    </div>
                    <form action="search.php" method="POST" class="header-top_search">
                        <input type="text" class="header-top_search-input" name="tukhoa" placeholder="Bạn tìm gì ...">
                        <input type="submit" name="timkiem" class="header-top_search_history_icon" value="Tìm kiếm" style="border: none; cursor: pointer;">
                        </input>
                    </form>
                        <!-- Search history -->
                        <!-- <div class="header-top_search_history">
                            <div class="header-top_search_history_heading_ground">
                            <h3 class="header-top_search_history_heading">
                                Lịch sử tìm kiếm
                            </h3>
                            </div>
                            <ul class="header-top_search_history_list">
                                <li class="header-top_search_history_item">
                                    <a href="">Laptop</a>
                                </li>
                                <li class="header-top_search_history_item">
                                    <a href="">Điện thoại</a>
                                </li>
                            </ul>
                        </div> -->
                    <!-- </div> -->
                    <div class="header-top_history">
                        <a href="">Lịch sử đơn
                        hàng</a>
                    </div>
                    <div class="header-top_cart">
                        <i class="fa-solid fa-cart-shopping"></i>
                        <a href=""><b>Giỏ hàng</b>
                        </a>
                    </div>
                    <div class="header-top_news">
                        <div class="header-top_news-item">
                            <a href="">
                                24h
                                <br> Công nghệ
                            </a>
                        </div>
                        <div class="border"></div>
                        <div class="header-top_news-item">
                            <a href="">
                                Hỏi
                                <br> đáp
                            </a>
                        </div>
                        <div class="border"></div>
                        <div class="header-top_news-item">
                            <a href="">
                                Game
                                <br> app
                            </a>
                        </div>
                    </div>
                </div>
                <div class="header-main">
                    <ul class="header-main-list">
                            <li class="header-main-item">
                                <a href="">
                                    <i class="fa-solid fa-mobile-screen"></i>
                                    Điện thoại
                                </a>
                            </li>
                            <li class="header-main-item">
                                <a href="">
                                    <i class="fa-solid fa-laptop"></i>
                                    Laptop
                                </a>
                            </li>
                            <li class="header-main-item">
                                <a href="">
                                    <i class="fa-solid fa-tablet-screen-button"></i>
                                    Tablet
                                </a>
                            </li>
                            <li class="header-main-item">
                                <a href="">
                                    <i class="fa-solid fa-headphones-simple"></i>
                                    Phụ kiện
                                    <i class="fa-solid fa-caret-down"></i>
                                </a>
                            </li>
                            <li class="header-main-item">
                                <a href="">
                                    <i class="fa-solid fa-watch-apple"></i>
                                    Smartwatch
                                </a>
                            </li>
                            <li class="header-main-item header-main-item-AVAJi">
                                <a href="">
                                    <i class="fa-solid fa-gem"></i>
                                    AVAJi
                                    <i class="fa-solid fa-caret-down"></i>
                                </a>
                                <ul class="item-detail-list">
                                        <li class="item-detail-one">
                                            <a href="">
                                                Đồng hồ thời trang
                                            </a>
                                        </li>
                                        <li class="item-detail-one">
                                            <a href="">
                                                Mắt kính
                                            </a>
                                        </li>
                                        <li class="item-detail-one">
                                            <a href="">
                                                Trang sức
                                            </a>
                                        </li>
                                </ul>
                            </li>
                            <li class="header-main-item">
                                <a href="">
                                    <i class="fa-solid fa-desktop"></i>
                                    PC, Máy in
                                    <i class="fa-solid fa-caret-down"></i>
                                </a>
                            </li>
                            <li class="header-main-item">
                                <a href="">
                                    Máy cũ giá rẻ
                                </a>
                            </li>
                            <li class="header-main-item">
                                <a href="">
                                    Sim, Thẻ cào
                                </a>
                            </li>
                            <li class="header-main-item">
                                <a href="">
                                    Dịch vụ tiện ích
                                    <i class="fa-solid fa-caret-down"></i>
                                </a>
                            </li>
                    </ul>
                </div>
                </div>
            </div>

            <div class="banner">
                <img src="./img/banner.png" alt="" style="height: 400px; width: 100%;">
            </div>

            <div class="main__content">
                <div class="slider_event">
                    <img style="width:97% !important;" class="border-radius" src="./img/sli1.png" alt="" >
                    <img class="border-radius" src="./img/sli2.png" alt="">
                    <img class="border-radius" src="./img/sli3.png" alt="">
                    <img class="border-radius" src="./img/sli4.png" alt="">
                    <img class="border-radius" src="./img/sli5.png" alt="">
                    <img class="border-radius" src="./img/sli6.png" alt="">
                </div>  

                <ul class="sale-options">
                    <li class="column-4">
                        <a href="">
                            <img src="./img/sale3.png" alt="">
                            <span>Chỉ giảm online</span>
                        </a>
                    </li>
                    <li class="column-4">
                        <a href="">
                            <img src="./img/sale4.png" alt="">
                            <span>Đồng giá từ 99k</span>
                        </a>
                    </li>
                    <li class="column-4">
                        <a href="">
                            <img src="./img/sale5.png" alt="">
                            <span>Xả hàng giảm sốc</span>
                        </a>
                    </li>
                    <li class="column-4">
                        <a href="">
                            <img src="./img/sale6.png" alt="">
                            <span>Bão sale lenovo</span>
                        </a>
                    </li>
                </ul>

                <img src="./img/sale7.png" alt="" style="margin: 16px 0px;">
                <div class="dealhot">
                    <img class="dealhot-img" src="./img/TGDD.png" alt="">
                    <div class="slider_dealhot row">
                        <div class="dealhot-product ">
                            <a href="detail_product.html">
                            <img class="dealhot-product__img" src="./img/dt1.jpg" alt="">
                            <h3>Asus TUF Gaming FA707RC R7 6800H</h3>
                            <strong class="price">27.990.000đ
                                <small class="price-small">-6%</small>
                            </strong>
                            </a>
                        </div>
                        <div class="dealhot-product ">
                            <a href="detail_product.html">
                            <img class="dealhot-product__img" src="./img/dt2.jpg" alt="">
                            <h3>Asus TUF Gaming FA707RC R7 6800H</h3>
                            <strong class="price">27.990.000đ
                                <small class="price-small">-6%</small>
                            </strong>
                            </a>
                        </div>
                        <div class="dealhot-product ">
                            <a href="detail_product.html">
                            <img class="dealhot-product__img" src="./img/dt3.jpg" alt="">
                            <h3>Asus TUF Gaming FA707RC R7 6800H</h3>
                            <strong class="price">27.990.000đ
                                <small class="price-small">-6%</small>
                            </strong>
                            </a>
                        </div>
                        <div class="dealhot-product ">
                            <a href="detail_product.html">
                            <img class="dealhot-product__img" src="./img/dt4.jpg" alt="">
                            <h3>Asus TUF Gaming FA707RC R7 6800H</h3>
                            <strong class="price">27.990.000đ
                                <small class="price-small">-6%</small>
                            </strong>
                            </a>
                        </div>
                        <div class="dealhot-product ">
                            <a href="detail_product.html">
                            <img class="dealhot-product__img" src="./img/dt5.jpg" alt="">
                            <h3>Asus TUF Gaming FA707RC R7 6800H</h3>
                            <strong class="price">27.990.000đ
                                <small class="price-small">-6%</small>
                            </strong>
                            </a>
                        </div>
                        <div class="dealhot-product">
                            <a href="detail_product.html">
                            <img class="dealhot-product__img" src="./img/dt1.jpg" alt="">
                            <h3>Asus TUF Gaming FA707RC R7 6800H</h3>
                            <strong class="price">27.990.000đ
                                <small class="price-small">-6%</small>
                            </strong>
                            </a>
                        </div>
                        <div class="dealhot-product ">
                            <a href="detail_product.html">
                            <img class="dealhot-product__img" src="./img/dt3.jpg" alt="">
                            <h3>Asus TUF Gaming FA707RC R7 6800H</h3>
                            <strong class="price">27.990.000đ
                                <small class="price-small">-6%</small>
                            </strong>
                            </a>
                        </div>
                        <div class="dealhot-product ">
                            <a href="detail_product.html">
                            <img class="dealhot-product__img" src="./img/dt4.jpg" alt="">
                            <h3>Asus TUF Gaming FA707RC R7 6800H</h3>
                            <strong class="price">27.990.000đ
                                <small class="price-small">-6%</small>
                            </strong>
                            </a>
                        </div>
                    </div>
                    <div class="btWatch">
                    <a href="" >Xem tất cả <i class="fa-solid fa-caret-right"></i></a>
                    </div>
                </div>

                <div class="trend">
                    <h3>XU HƯỚNG MUA SẮM</h3>
                    <ul class="trend-list">
                        <li class="trend-item">
                            <a href="">
                                <img src="./img/t1.png" alt="">
                            <span>Điện thoại</span>
                            <strong>Galaxy M Series</strong>
                            </a>                         
                        </li>
                        <li class="trend-item">
                            <a href="">
                                <img src="./img/t2.png" alt="">
                            <span>Điện thoại</span>
                            <strong>Galaxy M Series</strong>
                            </a>                         
                        </li>
                        <li class="trend-item">
                            <a href="">
                                <img src="./img/t3.png" alt="">
                            <span>Điện thoại</span>
                            <strong>Galaxy M Series</strong>
                            </a>                         
                        </li>
                        <li class="trend-item">
                            <a href="">
                                <img src="./img/t4.png" alt="">
                            <span>Điện thoại</span>
                            <strong>Galaxy M Series</strong>
                            </a>                         
                        </li>
                    </ul>
                    
                </div>

                <div class="hot-category">
                    <strong>DANH MỤC NỔI BẬT</strong>
                    <div class="hot-list-full">
                    <div class="hot-list">
                        <div class="hot-item">
                            <a href="">
                                <img src="./img/h1.png" alt="">
                                <span>Điện thoại</span>
                            </a>
                        </div>
                        <div class="hot-item">
                            <a href="">
                                <img src="./img/h2.png" alt="">
                                <span>Điện thoại</span>
                            </a>
                        </div>
                        <div class="hot-item">
                            <a href="">
                                <img src="./img/h3.png" alt="">
                                <span>Điện thoại</span>
                            </a>
                        </div>
                        <div class="hot-item">
                            <a href="">
                                <img src="./img/h4.png" alt="">
                                <span>Điện thoại</span>
                            </a>
                        </div>
                        <div class="hot-item">
                            <a href="">
                                <img src="./img/h5.png" alt="">
                                <span>Điện thoại</span>
                            </a>
                        </div>
                        <div class="hot-item">
                            <a href="">
                                <img src="./img/h6.png" alt="">
                                <span>Điện thoại</span>
                            </a>
                        </div>
                        <div class="hot-item">
                            <a href="">
                                <img src="./img/h7.png" alt="">
                                <span>Điện thoại</span>
                            </a>
                        </div>
                        <div class="hot-item">
                            <a href="">
                                <img src="./img/h8.png" alt="">
                                <span>Điện thoại</span>
                            </a>
                        </div>
                        <div class="hot-item">
                            <a href="">
                                <img src="./img/h9.png" alt="">
                                <span>Điện thoại</span>
                            </a>
                        </div>
                        <div class="hot-item">
                            <a href="">
                                <img src="./img/h10.png" alt="">
                                <span>Điện thoại</span>
                            </a>
                        </div>
                    </div>
                    <div class="hot-list">
                        <div class="hot-item">
                            <a href="">
                                <img src="./img/h10.png" alt="">
                                <span>Điện thoại</span>
                            </a>
                        </div>
                        <div class="hot-item">
                            <a href="">
                                <img src="./img/h9.png" alt="">
                                <span>Điện thoại</span>
                            </a>
                        </div>
                        <div class="hot-item">
                            <a href="">
                                <img src="./img/h8.png" alt="">
                                <span>Điện thoại</span>
                            </a>
                        </div>
                        <div class="hot-item">
                            <a href="">
                                <img src="./img/h7.png" alt="">
                                <span>Điện thoại</span>
                            </a>
                        </div>
                        <div class="hot-item">
                            <a href="">
                                <img src="./img/h6.png" alt="">
                                <span>Điện thoại</span>
                            </a>
                        </div>
                        <div class="hot-item">
                            <a href="">
                                <img src="./img/h5.png" alt="">
                                <span>Điện thoại</span>
                            </a>
                        </div>
                        <div class="hot-item">
                            <a href="">
                                <img src="./img/h4.png" alt="">
                                <span>Điện thoại</span>
                            </a>
                        </div>
                        <div class="hot-item">
                            <a href="">
                                <img src="./img/h3.png" alt="">
                                <span>Điện thoại</span>
                            </a>
                        </div>
                        <div class="hot-item">
                            <a href="">
                                <img src="./img/h2.png" alt="">
                                <span>Điện thoại</span>
                            </a>
                        </div>
                        <div class="hot-item">
                            <a href="">
                                <img src="./img/h1.png" alt="">
                                <span>Điện thoại</span>
                            </a>
                        </div>
                    </div>
                    </div>
                </div>

                <div class="pay-online">
                    <h3>GIẢM KHI THANH TOÁN ONLINE</h3>
                    <div class="slider_payList">
                        <div class="pay-item">
                            <img class="" src="./img/p1.png" alt="">
                        </div>
                        <div class="pay-item">
                            <img class="" src="./img/p2.png" alt="">
                        </div>
                        <div class="pay-item">
                            <img class="" src="./img/p3.png" alt="">
                        </div>
                        <div class="pay-item">
                            <img class="" src="./img/p4.png" alt="">
                        </div>
                        <div class="pay-item">
                            <img class="" src="./img/p5.png" alt="">
                        </div>
                        <div class="pay-item">
                            <img class="" src="./img/p6.png" alt="">
                        </div>
                    </div>
                </div>

                <div class="service">
                    <h3>DỊCH VỤ TIỆN ÍCH</h3>
                    <ul class="service-list">
                        <li class="service-item">
                           <div class="service-icon">
                                <p class="service-img"></p>
                           </div> 
                           <div class="service-text">
                                <small class="service-text__head">Mua mã thẻ cào</small>
                                <small>
                                    <strong>Giảm 3%</strong>
                                     cho mệnh giá 100.00 trở lên
                                </small>
                            </div>                       
                        </li>
                        <li class="service-item yellow">
                            <div class="service-icon">
                                 <p class="service-img"></p>
                            </div> 
                            <div class="service-text">
                                 <small class="service-text__head">Mua mã thẻ cào</small>
                                 <small>
                                     <strong>Giảm 3%</strong>
                                      cho mệnh giá 100.00 trở lên
                                 </small>
                             </div>                       
                         </li>
                         <li class="service-item orange">
                            <div class="service-icon">
                                 <p class="service-img"></p>
                            </div> 
                            <div class="service-text">
                                 <small class="service-text__head">Mua mã thẻ cào</small>
                                 <small>
                                     <strong>Giảm 3%</strong>
                                      cho mệnh giá 100.00 trở lên
                                 </small>
                             </div>                       
                         </li>
                         <li class="service-item green">
                            <div class="service-icon">
                                 <p class="service-img"></p>
                            </div> 
                            <div class="service-text">
                                 <small class="service-text__head">Mua mã thẻ cào</small>
                                 <small>
                                     <strong>Giảm 3%</strong>
                                      cho mệnh giá 100.00 trở lên
                                 </small>
                             </div>                      
                         </li>
                    </ul>                  
                </div>

                <div class="suggest">
                    <div>
                        <h3>GỢI Ý HÔM NAY</h3>
                    </div>
                    <ul class="suggest-options">
                        <li class="column-4" style="background-color: rgb(226, 138, 37);">
                            <a href="">
                                <img src="./img/s2.png" alt="">
                                <span>Cho bạn</span>
                            </a>
                        </li>
                        <li class="column-4">
                            <a href="">
                                <img src="./img/s3.gif" alt="">
                                <span>Giá rẻ</span>
                            </a>
                        </li>
                        <li class="column-4">
                            <a href="">
                                <img src="./img/s4.png" alt="">
                                <span>Chỉ giảm online</span>
                            </a>
                        </li>
                        <li class="column-4">
                            <a href="">
                                <img src="./img/s5.png" alt="">
                                <span>Deal từ 99k</span>
                            </a>
                        </li>
                    </ul>
                    <div class="suggest-listproduct" style="flex-wrap: wrap;">
                        <?php if(count($phones) > 0) { ?>     
                            <?php foreach ($phones as $phones) {?>
                            <div class="suggest-product column-5" style="width = 18.5% !important;">    
                                <a href="./detail_product.php?id=<?= $phones['idPhone'] ?>">                     
                                <img class="suggest-product__img" src="./img/dt1.jpg" alt="">
                                </a> 
                                <h3><?php echo $phones['name'] ?></h3>
                                <strong class="price"><?php echo number_format($phones['price'],0,",",".") ?>
                                    <small class="price-small">-6%</small>
                                </strong>
                            </div>
                        <?php } ?>
                        <?php } else{ ?>
                        <h2>No Data.</h2>
                        <?php } ?>
                    </div>
                    <div style="clear:both;"></div>
                    <style type="text/css">
                        .list_trang{
                            float: right;
                        }
                        ul.list_trang{
                            padding: 0;
                            margin: 0;
                            list-style: none;
                        }
                        ul.list_trang li{
                            float: left;                           
                            margin: 3px;                           
                            display: block;
                            text-align: center;
                            padding-top: 5px;
                        }
                        ul.list_trang li a {
                            text-decoration: none;
                            color: #000;
                            text-align: center;
                            padding: 5px 13px;
                            background: burlywood;
                        }
                    </style>
                    
                    <ul class="list_trang">    
                        <?php for($i=1;$i<=$number_page;$i++){ ?>             
                            <li><a
                        <?php 
                        if($i==$current_page) {echo 'style="background:brown"';}
                        else {echo '';} ?> href="index2.php?trang=<?php echo $i ?>"><?php echo $i ?></a></li>
                        <?php }  ?>
                    </ul>
                    <div class="btWatch">
                        <a href="" >Xem thêm
                    </div>
                </div>

                
            </div>
            
            <div class="footer">
                <section class="footer-top">
                    <div class="coloumn-4" style="margin-right: 60px;">
                        <ul class="footer-list">
                            <li>
                                <a href="">Tích điểm quà tặng VIP</a>
                            </li>
                            <li>
                                <a href="">Lịch sử mua hàng</a>
                            </li>
                            <li>
                                <a href="">Cộng tác bán hàng cùng TGDĐ</a>
                            </li>
                            <li>
                                <a href="">Tìm hiểu về mua trả góp</a>
                            </li>
                            <li>
                                <a href="">Chính sách bảo hành</a>
                            </li>
                        </ul>
                    </div>
                    <div class="coloumn-4" style="margin-right: 60px;">
                        <ul class="footer-list">
                            <li>
                                <a href="">Giới thiệu công ty</a>
                            </li>
                            <li>
                                <a href="">Tuyển dụng</a>
                            </li>
                            <li>
                                <a href="">Gửi góp ý, khiếu nại</a>
                            </li>
                            <li>
                                <a href="">Tìm siêu thị(3.165 shop)</a>
                            </li>
                            <li>
                                <a href="">Xem bản mobile</a>
                            </li>
                        </ul>
                    </div>
                    <div class="coloumn-4" style="margin-right: 90px;" >
                        <ul class="footer-list" >
                            <li>
                                <p class="f3-title">
                                    <strong>Tổng đài hỗ trợ</strong>
                                     (Miễn phí gọi)
                                </p>
                            </li>
                            <li>
                                <p class="f3-content">
                                    <span>Gọi mua:</span>
                                    <a href="tel: 18001060">1800.1060</a> (7:30 - 22:00)
                                </p>
                            </li>
                            <li>
                                <p class="f3-content">
                                    <span>Kĩ thuật:</span>
                                    <a href="tel: 18001060">1800.1060</a> (7:30 - 22:00)
                                </p>
                            </li>
                            <li>
                                <p class="f3-content">
                                    <span>Khiếu nại:</span>
                                    <a href="tel: 18001060">1800.1060</a> (7:30 - 22:00)
                                </p>
                            </li>
                            <li>
                                <p class="f3-content">
                                    <span>Bảo hành:</span>
                                    <a href="tel: 18001060">1800.1060</a> (7:30 - 22:00)
                                </p>
                            </li>
                        </ul>
                    </div>
                    <div class="coloumn-4">
                        <ul class="footer-list">
                            <li>
                                <div class="footer-social">
                                    <a href="" style="color:rgb(37, 148, 245) !important;">
                                        <i class="fa-brands fa-facebook" style="color:rgb(3, 69, 223);"></i>
                                         100.99k Fan
                                    </a>
                                    <a href="" style="margin-left: 10px; color:rgb(37, 148, 245) !important;">
                                        <i class="fa-brands fa-youtube" style="color:red;"></i>
                                        800k Đăng ký
                                    </a>
                                </div>
                            </li>
                            <li>
                                <div class="f-certify">
                                    <a href="">
                                        <i class="icon-congthuong"></i>
                                    </a>
                                    <a href="">
                                        <i class="icon-khieunai"></i>
                                    </a>
                                    <a href="">
                                        <i class="icon-protected"></i>
                                    </a>
                                    <a href="" ">
                                        <img style="width: 75px; margin-left: 5px;" src="./img/n1.png" alt="">
                                    </a>
                                </div>
                            </li>
                            <li>
                                <div class="footer-logo">
                                    <p>Website cùng tập đoàn</p>
                                    <div class="f-certify">
                                        <a href="">
                                            <i class="icon-topzone"></i>
                                        </a>
                                        <a href="">
                                            <i class="logo-dienmayxanh"></i>
                                        </a>
                                        <a href="">
                                            <i class="icon-protected"></i>
                                        </a>
                                        <a href="" ">
                                            <img style="width: 75px; margin-left: 5px;" src="./img/n1.png" alt="">
                                        </a>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </section>
                <div class="source">
                    © 2022. Pham Thi Ngoc Diep 
                </div>
            </div>
        </div>
        <script type="text/javascript" src="./js/js2.js"></script>
    </body>
</html>
