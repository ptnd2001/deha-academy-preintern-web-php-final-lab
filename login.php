<!DOCTYPE html>
<html>
<head>
	<title>Login Page</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    
    <!--Fontawesome CDN-->
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

	<!--Custom styles-->
	<link rel="stylesheet" type="text/css" href="./asscess/css/Csslogin.css">
    <style>
        .h-100{
            height: 40%!important;
            margin: 30px;
        }
        .card{
            min-width: 600px;
        }
    </style>
</head>
<body>
<div class="container">
	<div class="d-flex justify-content-center h-100">
		<div class="card">
			<div class="card-header">
				<h3>Đăng nhập</h3>
			</div>
			<div class="card-body">
				<form method="POST"> 
					<div class="input-group form-group">
						<div class="input-group-prepend">
							<span class="input-group-text"><i class="fas fa-user"></i></span>
						</div>
						<input type="text" name="username" class="form-control" placeholder="Tên đăng nhập">						
					</div>
					<div class="input-group form-group">
						<div class="input-group-prepend">
							<span class="input-group-text"><i class="fas fa-key"></i></span>
						</div>
						<input type="password" name="password" class="form-control" placeholder="Mật khẩu">
					</div>
					<div class="form-group">
						<input type="submit" name="dangnhap" value="Login" class="btn float-right login_btn">
					</div>
				</form>
			</div>
			<?php
    		include_once "./db/user.php";

			// include_once "./db/Auth.php";
			// if (isset($_POST['dangnhap'])){
			// 	$username = addslashes($_POST['username']);
			// 	$password = addslashes($_POST['password']);
			// 	if (!$username || !$password) {
			// 		echo "Vui lòng nhập đầy đủ tên đăng nhập và mật khẩu.";
			// 		}
			// 	else{
			// 		$user = Auth::login($username);

			// 	}
			// }

			?>
			
			<div class="card-footer">
			<div class="d-flex justify-content-center">
					<a href="register.php">Đăng ký tài khoản mới?</a>
				</div>
			</div>
		</div>
	</div>
</div>
<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
</body>
</html>