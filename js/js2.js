
let statusText = 'Unfilter Slides';  
    $('.slider-banner').slick({
            slidesToShow: 1,
            slidesToScroll: 1
            });
        var filtered = false;
        $('.js-filter').on('click', function(){
        if (filtered) {
            $('.slider-banner').slick('slickUnfilter');
            statusText = 'Filter Slides';
            filtered = false;      
        } else {
            $('.slider-banner').slick('slickFilter',':even');       
            filtered = true;
        }
        $(this).text(statusText);
        });


    $('.slider_event').slick({
            slidesToShow: 2,
            slidesToScroll: 2,
            autoplay: true,
            autoplaySpeed: 1700,
            });
        var filtered;
        $('.js-filter').on('click', function(){
        if (filtered) {
            $('.slider_event').slick('slickUnfilter');
            statusText = 'Filter Slides';
            filtered = false;        
        } else {
            $('.slider_event').slick('slickFilter',':even');
            filtered = true;
        }
        $(this).text(statusText);
        });
        
         
    $('.slider_dealhot').slick({
            slidesToShow: 5,
            slidesToScroll: 4
            });

        var filtered = false;
        $('.js-filter').on('click', function(){
            
        if (filtered) {
            $('.slider_dealhot').slick('slickUnfilter');
            statusText = 'Filter Slides';
            filtered = false;       
        } else {
            $('.slider_dealhot').slick('slickFilter',':even');
            filtered = true;
        }
        $(this).text(statusText);
        });

        
    const autoSpeed = 3000;
    $('.slider_payList').slick({
        slidesToShow: 3,
        slidesToScroll: 3,
        autoplay: true,
        autoplaySpeed: autoSpeed,
    });
    var filtered = false;
    $('.js-filter').on('click', function(){
    if (filtered) {
        $('.slider_payList').slick('slickUnfilter');
        statusText = 'Filter Slides';
        filtered = false;    
    } else {
        $('.slider_payList').slick('slickFilter',':even');
        filtered = true;
    }
    $(this).text(statusText);
    });

